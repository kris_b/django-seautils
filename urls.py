#coding: utf-8

PAGINATION_PATTERN = 'page(?P<page_nr>\d+)'
SLUG_PATTERN =  '[a-zA-Z0-9\-\+_\.\?]+'
