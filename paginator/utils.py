#coding: utf-8

def pagination_range(current_page, num_pages, max_links):
    """
    Method returns sliced links list.
    """
    
    if num_pages > 1:
        if num_pages <= max_links:
            pagination_range = range(1, num_pages + 1)
        else:
            pagination_range = range(max(1, current_page - (max_links - 1)/2) , 
                                     min(num_pages, current_page + (max_links - 1)/2) + 1)
    else:
        pagination_range = []
    
    return pagination_range
