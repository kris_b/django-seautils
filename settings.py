# -*- coding: utf-8 *-*
import re
from functools import partial

SETTINGS_ATTRIBUTE_PATTERN = re.compile(r'^[A-Z_]+$')
SETTINGS_VAR_PATTERN = re.compile('\!_([A-Z_]+)_\!')


class DelayedSettingsKeyError(Exception):
    pass


def finalize(settings_locals):
    set_email_prefix(settings_locals)
    calculate_final_values(settings_locals)


def _replace_func(matchobj, settings_locals):
    try:
        return str(settings_locals[matchobj.group(1)])
    except KeyError:
        raise DelayedSettingsKeyError("There is no setting called %s."
                                      " in your settings (used in %s)."\
                                       % (matchobj.group(1), matchobj.group(0)))


def calculate_final_values(settings_locals):
    replace_func = partial(_replace_func, settings_locals=settings_locals)
    for key, val in settings_locals.items():
        if not SETTINGS_ATTRIBUTE_PATTERN.match(key):
            continue

        if val.__class__ == str or val.__class__ == unicode:
            settings_locals[key] = SETTINGS_VAR_PATTERN.sub(replace_func, val)
            if re.match(r'^[0-9]+$', settings_locals[key]):
                settings_locals[key] = int(settings_locals[key])


def set_email_prefix(settings_locals):
    labels = []
    try:
        import socket
        labels.append(str(socket.gethostname().replace('.', '_')))
    except:
        labels.append('UnknownHost')

    if settings_locals.has_key('INSTANCE_LABEL'):
        labels.append(settings_locals['INSTANCE_LABEL'])

    settings_locals['EMAIL_SUBJECT_PREFIX'] = "[%s] " % ' | '.join(labels)


